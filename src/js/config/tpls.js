import * as tpl from 'components/HousesTpls';

const tpls = [
  {
    id: 1,
    template: [
      {
        component: tpl.Images,
        field: 'images',
      }, {
        component: tpl.Text,
        field: 'full_address',
        name: "Address"
      }, {
        component: tpl.Text,
        field: 'price',
        name: "Price"
      }, {
        component: tpl.Text,
        field: 'rating',
        name: "Rating"
      }, {
        component: tpl.Text,
        field: 'area',
        name: "Area"
      }
    ]
  }, {
    id: 2,
    template: [
      {
        component: tpl.Text,
        field: 'full_address',
        name: "Address"
      }, {
        component: tpl.Images,
        field: 'images',
      }, {
          component: tpl.Text,
          field: 'description',
          name: "Description"
      }, {
        component: tpl.Text,
        field: 'price',
        name: "Price"
      }, {
        component: tpl.Text,
        field: 'rating',
        name: "Rating"
      }, {
        component: tpl.Text,
        field: 'area',
        name: "Area"
      }
    ]
  }, {
    id: 3,
    template: [
      {
        component: tpl.Text,
        field: 'full_address',
        name: "Address"
      }, {
        component: tpl.Images,
        field: 'images',
        children: [
          {
            component: tpl.Text,
            field: 'price',
            name: 'Price',
          }, {
            component: tpl.Text,
            field: 'rating',
            name: 'Rating',
          }
        ]
      },
      {
        component: tpl.Text,
        field: 'area',
        name: 'Area'
      }
    ]
  }
];

export default tpls;