## House Map

### `git clone https://gitlab.com/andrew.velkov/housemap.git`

### `cd housemap`

### `npm install`

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
to view it in the browser.
Open App [http://localhost:8080](http://localhost:8080)

Prod
### `npm run build`