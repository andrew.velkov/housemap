const templates = [
  {
    id: 1,
    value: 'Template 1',
  },
  {
    id: 2,
    value: 'Template 2',
  },
  {
    id: 3,
    value: 'Template 3',
  },
];

export default templates;
