import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';

import { getHouseMap } from 'actions';
import Fetching from 'components/Fetching';
import Select from 'components/Form/Select';
import HouseTplContainer from '../HouseTplContainer';

import templates from 'config/templates';

import css from 'styles/containers/HouseMap.scss';

class HouseMapContainer extends Component {
  state = {
    template: 1,
  }

  componentDidMount() {
    this.props.getHouseMap();
  }

  handleChange = (e) => {
    const { name, value } = e.target;

    this.setState({
      [name]: value
    });
  }

  render() {
    const { data, loading, loaded } = this.props.houseMap;
    const { template } = this.state;
    const houses = loaded && data.data;

    return (
      <section className={css.wrap}>
        <Select
          title="Select template"
          name="template"
          data={templates}
          value={template}
          onChange={this.handleChange}
        />

        <Fetching isFetching={loading}>
          <Grid container spacing={16}>
            {houses && houses.map(house => {
              return (
                <Grid item xs={12} sm={2} md={3} key={house.id}>
                  <HouseTplContainer
                    house={house}
                    selectedTemplate={template}
                  />
                </Grid>
              );
            })}
          </Grid>
        </Fetching>
      </section>
    );
  }
}

export default connect(state => ({
  houseMap: state.get.houseMap,
}), dispatch => ({
  getHouseMap: () => dispatch(getHouseMap()),
}))(HouseMapContainer);
