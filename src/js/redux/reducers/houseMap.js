const initialState = {
  loaded: false,
  loading: false,
  data: [],
};

export default function movie(params = '') {
  return (state = initialState, action = {}) => {
    switch (action.type) {
      case `map/${params}/LOAD`:
        return {
          ...state,
          loading: true,
          loaded: false,
        };
      case `map/${params}/LOAD_SUCCESS`:
        return {
          ...state,
          loading: false,
          loaded: true,
          data: action.payload,
        };
      case `map/${params}/LOAD_ERROR`:
        return {
          ...state,
          loading: false,
          loaded: false,
        };
      default:
        return state;
    }
  };
};