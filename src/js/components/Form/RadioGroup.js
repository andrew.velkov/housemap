import React from 'react';
import PropTypes from 'prop-types';
import {
  FormControl, FormLabel, RadioGroup as RadioGroups, FormControlLabel, Radio
} from '@material-ui/core';

const RadioGroup = ({ name, data, title, value, onChange }) => (
  <FormControl component="fieldset">
    <FormLabel component="legend">{title}</FormLabel>
    <RadioGroups
      name={name}
      value={value}
      onChange={onChange}
      style={{display: 'inline-block'}}
    >
      {data.map(item => {
        return (
          <FormControlLabel
            key={item.id}
            value={item.value}
            label={item.name}
            control={<Radio color="primary" />}
            style={{paddingRight: '15px'}}
          />
        );
      })}
    </RadioGroup>
  </FormControl>
);

RadioGroup.propTypes = {
  onChange: PropTypes.func,
};

export default RadioGroup;
