import React, { Component } from 'react';
import Header from 'components/Header';

class App extends Component {
  render() {
    const { children } = this.props;

    return (
      <main>
        <Header />
        {children}
      </main>
    );
  }
}

export default App;
