import { combineReducers } from 'redux';

import houseMap from './houseMap';

const reducers = combineReducers({
  get: combineReducers({
    houseMap: houseMap('house'),
    tpl: houseMap('tpl'),
  }),
});

export default reducers;
