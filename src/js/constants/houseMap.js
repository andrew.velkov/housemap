export const GET_HOUSEMAP = 'map/house/LOAD';
export const GET_HOUSEMAP_SUCCESS = 'map/house/LOAD_SUCCESS';
export const GET_HOUSEMAP_ERROR = 'map/house/LOAD_ERROR';

export const GET_TPL = 'map/tpl/LOAD';
export const GET_TPL_SUCCESS = 'map/tpl/LOAD_SUCCESS';
export const GET_TPL_ERROR = 'map/tpl/LOAD_ERROR';
