import React from 'react';

const Images = ({ value, children }) => (
  <ul>
    <li>
      {children}
      <img src={value[0]} width="100px" alt="" />
    </li>
  </ul>
);

export default Images;
