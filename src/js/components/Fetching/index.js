import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';

import css from '../../../styles/components/Fetching.scss';

const Fetching = ({ color = 'secondary', size = 45, thickness = 3, isFetching, children }) => (
  <div className={ css.fetching }>
    {isFetching && <div className={ css.fetching__wrap }>
      <CircularProgress color={ color } size={ size } thickness={ thickness } />
    </div>}
    {children}
  </div>
);

Fetching.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  thickness: PropTypes.number,
  isFetching: PropTypes.bool,
  children: PropTypes.any,
};

export default Fetching;
