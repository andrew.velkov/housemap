import React from 'react';
import { Link } from 'react-router-dom';

import styles from 'styles/pages/NotFound.scss';

const NotFoundPage = () => (
  <section className={styles.notFound}>
    <article className={styles.notFound__inner}>
      <h2>Page not found! <Link to="/"> Back to home</Link></h2>
    </article>
  </section>
);

export default NotFoundPage;
