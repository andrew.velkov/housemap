import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from 'containers/App';
import HouseMapPage from 'pages/HouseMapPage';
import NotFoundPage from 'pages/NotFoundPage';

class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <App>
          <Switch>
            <Route exact path='/' render={props => <HouseMapPage />} />
            <Route component={ NotFoundPage } />
          </Switch>
        </App>
      </BrowserRouter>
    );
  }
}

export default Routes;
