import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

const Input = ({ label, name, value, onChange, error, ...input }) => (
  <TextField
    label={label}
    name={name}
    value={value}
    fullWidth
    margin="normal"
    placeholder=''
    onChange={ onChange }
    {...input}
  />
);

Input.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
};

export default Input;
