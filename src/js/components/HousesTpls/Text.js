import React from 'react';

const Text = ({ value, name }) => {
  return (
    <React.Fragment>
      {value !== undefined && <p>
        <b>{name}</b>: <i>{value}</i>
    </p>}
    </React.Fragment>
  );
};

export default Text;
