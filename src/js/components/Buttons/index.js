import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

const Buttons = ({ variant = 'contained', color = 'primary', children, onClick, ...button }) => (
  <React.Fragment>
    <Button variant={variant} color={color} onClick={onClick} {...button}>
      {children}
    </Button>

    {variant === '' && <Button color={color} onClick={onClick} {...button}>
      {children}
    </Button>}
  </React.Fragment>
);

Buttons.propTypes = {
  variant: PropTypes.string,
  children: PropTypes.any,
  onClick: PropTypes.func,
};

export default Buttons;
