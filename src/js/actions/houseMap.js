import * as type from 'constants/houseMap';
import API from 'config/api';

export const getHouseMap = () => {
  return {
    types: [type.GET_HOUSEMAP, type.GET_HOUSEMAP_SUCCESS, type.GET_HOUSEMAP_ERROR],
    request: {
      method: 'GET',
      url: `${API.base_url}/properties`,
    },
  };
};

// export const getTpls = () => {
//   return {
//     types: [type.GET_TPL, type.GET_TPL_SUCCESS, type.GET_TPL_ERROR],
//     request: {
//       method: 'GET',
//       url: `${API.base_url}/templates`,
//     },
//   };
// };
