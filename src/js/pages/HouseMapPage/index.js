import React from 'react';
import HouseMapContainer from 'containers/HouseMapContainer';

const HouseMapPage = () => <HouseMapContainer />

export default HouseMapPage;
