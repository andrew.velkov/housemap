const apiMiddleWare = () => next => action => {
  const { types, request: { method = 'GET', url, body } } = action;

  if (!types) {
    return next(action);
  }

  const [REQUEST, SUCCESS, FAILURE] = types;

  next({ type: REQUEST });

  return fetch(url, {
    method,
    headers: {
      'Accept': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        return response.json();
      }

      if (response.status === 401) {
        return next({ ...rest, type: FAILURE });
      }

      return response.json();
    })
    .then(data => next({
      type: SUCCESS,
      payload: data,
    }))
    .catch(error => next({
      type: FAILURE,
      payload: error.message,
    }));
};

export default apiMiddleWare;
