import React, { Component } from 'react';

import tpls from 'config/tpls';

class HouseMapContainer extends Component {
  render() {
    const { selectedTemplate, house } = this.props;
    const selecedTpls = tpls.find(item => item.id === selectedTemplate);

    return (
      <React.Fragment>
        {selecedTpls.template.map(item => {
          const Component = item.component;
          const key = Object.keys(house).find(key => key === item.field);

          return (
            <Component key={item.field} name={item.name} value={house[key]}>
              {item.children && item.children.map(child => {
                const ChildComponent = child.component;

                return (
                  <ChildComponent key={child.field} name={child.name} value={house[child.field]} />
                );
              })}
            </Component>
          ); 
        })}
      </React.Fragment>
    );
  }
}

export default HouseMapContainer;
